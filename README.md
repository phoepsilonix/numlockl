# numlockl

numlockl is a command-line programme that retrieves the state of the numlock button or switches its state on and off.

## Installation

```bash
make install
```

## Usage

```bash
numlockl
numlockl on
numlockl off
numlockl toggle
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
