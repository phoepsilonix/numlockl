TARGET  = numlockl
SRCS = numlockl.c strlcat.c
OBJS = $(SRCS:%.c=%.o)
LIBS = 
CPPFLAGS+= -Wall
LDFLAGS+= 
prefix = /usr
exec_prefix = ${prefix}
bindir=${exec_prefix}/bin
INSTALL= /usr/bin/install


$(TARGET): $(OBJS)
	$(CC) -o $@ $^ $(LIBS) $(LDFLAGS)

.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f $(OBJS)

install: $(TARGET)
	$(INSTALL) -Ds $(TARGET) $(bindir)/$(TARGET)

uninstall:
	rm -i $(bindir)/$(TARGET)


